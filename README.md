# python avro schema generator

Generate avro schemas with reusable parts.

Adapted to python from here: http://www.treselle.com/blog/advanced-avro-schema-design-reuse/

Documentation server is a docker image based on AVRODOC: https://github.com/ept/avrodoc

## Installation
run: pip install -r requirements.txt

## How to set up
* if you want to deploy documentation server:
    * replace URL_OF_AD_SERVER in changes/nginx.conf if you want to use AD for user management
    * deploy docker container in `docker_avrodoc` 
* deploy schema repo server with Dockerfile in `docker_schema_repo`

## Usage

### define new schema / change existing schema
the notebook "AVRO Schema Editor" guides you through changing and creating schemas
step by step

### generate new and change existing schema with replacements
the notebook "AVRO Schema Generator" implements all functions. You can:
* generate all schemas defined in "./schemas/defined"
* generate on specific schema defined in "./schemas/defined"
* upload schemas to the schema repository