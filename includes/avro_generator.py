import avro
import avro.io
import avro.datafile

import json
import os
import requests


class AvroGenerator(object):

    def __init__(self):
        self._schema_repo_base = 'schema-repo'

    def check_schema(self, path_to_schema):
        """
        this function checks, if a schema is valid.
        @parameters
            path_to_schema (str): path to schema to be parsed
        @returns:
            boolean for validity
            parsed avro schema
        """
        try:
            json_file = open(path_to_schema)
            schema = avro.schema.Parse(json.dumps(json.load(json_file)))
            success = True
        except Exception as e:
            print (str(e))
            success = False
            schema = ""
        return success, schema

    def add_schema_to_map(self, path_to_data_type_directory):
        """
        This function parses the avro schema datatypes into a map, to make them reusable.
        @param:
        - path_to_data_type_directory (string): path to directory where defined data types are located

        @return:
            dictionary of schemas
        """
        subschemas = {}
        directory = os.fsencode(path_to_data_type_directory)
        for file in os.listdir(directory):
            success, schema = self.check_schema(path_to_data_type_directory + '/' + file.decode('utf-8'))
            if success == True:
                schema_name = (schema.namespace + '.' + schema.name)
                subschemas[schema_name] = schema
            else:
                print ("Schema " + path_to_data_type_directory + '/' + file.decode('utf-8') + " is invalid.")
        return subschemas

    def replace_subschemas_in_schema(self, path_to_schema, path_to_data_type_directory):
        """
        This function replaces placeholders in a schema with that valid avro schemas that
        are saved in the @parameter "path_to_data_type_directory" and returns a valid schema

        @parameter:
        - path_to_schema (string):               path to schemas that contains placeholders
        - path_to_data_type_directory (string):  path to directory containing the predifined 
                                                 reusable parts for schemas

        @returns:
        - success: boolean with information if schema was created sucessfully
        - schema:  a valid avro schema
        """
        subschemas = self.add_schema_to_map(path_to_data_type_directory)
        json_file = open(path_to_schema)
        json_dict = json.load(json_file)
        i = 0
        for a in json_dict['fields']:
            for key in subschemas.keys():
                if a['type'] == key:
                    json_dict['fields'][i]['type'] = str(subschemas[key])
            i = i + 1
        json_dict = json.loads(str(json_dict).replace("'{", "{").replace("}'", "}").replace("'", '"'))

        i = 0
        for field in json_dict['fields']:
            for key, value in field.items():
                if value == "null":
                    json_dict['fields'][i][key] = None
            i = i + 1

        try:
            schema = avro.schema.Parse(json.dumps(json_dict))
            success = True
        except:
            success = False
            schema = ""
        return success, schema

    def write_avro_to_file(self, schema, path_to_schema='./schemas/generated/default.avsc'):
        """
        This function write the generated avro schema to a file
        @parameters:
        - schema (avro schema): correctly created avro schema
        - path_to_schema (str): path with filename, where schema should be saved 
        """
        try:
            file = open(path_to_schema, 'w')
            file.write(str(schema))
            file.close()
        except BaseException:
            raise Exception("schema could not be written to file")

    def generate_all_schemas(self, path_to_all_schemas='./schemas'):
        """
        This function generates all schemas that are available in the "path_to_all_schemas/defined"
        directory. All generated schemas are saved to the directory "generated"
        @parameters:
        - path_to_all_schemas (str): path of the directory, that contains the subfolders:
                                     - defined
                                     - data_types
                                     - generated
        """
        directory = os.fsencode(path_to_all_schemas + '/defined')
        for file in os.listdir(directory):
            self.generate_specific_schema(file.decode('utf-8'), path_to_all_schemas=path_to_all_schemas)

    def generate_specific_schema(self, schema_name, path_to_all_schemas='./schemas'):
        """
        This function generates one specific schema, given by the parameter "schema_name" and saves it
        to the directory "generated"
        @parameter:
        - schema_name (str):         name of schema file that should be generated
        - path_to_all_schemas (str): path of the directory, that contains the subfolders:
                                     - defined
                                     - data_types
                                     - generated
        """
        schema_file_name = path_to_all_schemas + '/defined/' + schema_name
        path_to_data_type_directory = path_to_all_schemas + '/data_types'
        success, schema = self.replace_subschemas_in_schema(schema_file_name, path_to_data_type_directory)
        if success == True:
            path_to_generated_schema = path_to_all_schemas + '/generated/' + schema_name
            self.write_avro_to_file(schema, path_to_schema=path_to_generated_schema)
        else:
            print ("File was not written: " + schema_name)

    def put_schema_to_registry(self, schema_name='some_schema.avsc', schema_repo_url='localhost:2876',
                               path_to_all_schemas='./schemas'):
        """

        :param schema_name:
        :param schema_repo_url:
        :param path_to_all_schemas:
        :return:
        """
        # open schema_file
        response = ""
        try:
            file = open(path_to_all_schemas + '/generated/' + schema_name)
            schema = file.read()
            # print (schema)
            headers = {'Content-Type': 'application/x-www-form-urlencoded'}
            schema_list = self.get_all_schemas_from_registry(schema_repo_url)

            if schema_name in schema_list:
                self.put_new_schema_to_registry(schema_name=schema_name, schema_repo_url=schema_repo_url, schema=schema)
            else:
                response = requests.put(
                    'http://' + schema_repo_url + '/' + self._schema_repo_base + '/' + schema_name,
                    data='',
                    headers=headers
                )
                self.put_new_schema_to_registry(schema_name=schema_name, schema_repo_url=schema_repo_url, schema=schema)
        except IOError as e:
            response = str(e)

        return response

    def put_new_schema_to_registry(self, schema_name='some_schema', schema_repo_url='localhost:2876', schema=''):
        headers = {'Content-Type': 'text/plain'}
        response = requests.put('http://' + schema_repo_url + '/' + self._schema_repo_base + '/' + schema_name
                                + '/register',
                                data=schema,
                                headers=headers
                                )

    def get_schema_from_registry(self, schema_name='some_schema', schema_repo_url='localhost:2876'):
        """

        :param schema_name:
        :param schema_repo_url:
        :return:
        """
        schema_list = self.get_all_schemas_from_registry(schema_repo_url)

        if schema_name in schema_list:
            response = requests.get('http://' + schema_repo_url + '/' + self._schema_repo_base + '/' + schema_name
                                    + '/latest')

            return response.text[2:]
        else:
            return 'No schema of this name available'

    def get_all_schemas_from_registry(self, schema_repo_url='localhost:2876'):
        """

        :param schema_repo_url:
        :return:
        """
        response = requests.get('http://' + schema_repo_url + '/' + self._schema_repo_base + '/')

        return response.text.split('\n')[:-1]

    def put_all_schemas_to_registry(self, path_to_all_schemas='./schemas', schema_repo_url='localhost:2876'):
        """

        :param path_to_all_schemas:
        :param schema_repo_url:
        :return:
        """
        directory = os.fsencode(path_to_all_schemas + '/defined')
        for file in os.listdir(directory):
            self.put_schema_to_registry(schema_name=file.decode('utf-8'), schema_repo_url=schema_repo_url,
                                        path_to_all_schemas=path_to_all_schemas)

    def check_correct_default_value(self, schema):
        """
        function checks, if default value is correctly defined, if one is there.
        :param schema: dict of avro schema
        :return: tuple of boolean and string message
        """
        fields = schema['fields']

        b_res = True
        msg = ''

        for field in fields:
            print(field)
            if 'default' in field:
                if field['default'] == 'null':
                    b_res = True
                    msg = ''
                else:
                    b_res = False
                    print('Default: ' + field['default'])
                    msg = field['default']

        return b_res, msg
