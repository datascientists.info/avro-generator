import unittest

from includes.avro_generator import AvroGenerator

class TestAvro(unittest.TestCase):
    def test_check_correct_default_value(self):
        ag = AvroGenerator()
        schema = {
            'fields': [
                {
                    'name': 'test',
                    'type': 'string',
                    'default': 'int'
                }
            ]
        }
        res, msg = ag.check_correct_default_value(schema)

        self.assertEquals(False, res)
