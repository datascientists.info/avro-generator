FROM openjdk:8u171
LABEL info.datescientists.data.schema.repo="1.0.0"
LABEL info.datescientists.data.schema.release_date="2018-08-23"
MAINTAINER Marc Matt

# Clone GitHub Repository
RUN apt-get update
RUN apt-get install git
RUN apt-get -y install maven
RUN git clone -b master https://github.com/schema-repo/schema-repo.git /schema-repo/
RUN useradd -ms /bin/bash schema_repo
RUN cp -R /schema-repo/* /home/schema_repo/

RUN chown schema_repo:schema_repo -R /home/schema_repo/

ENV JAVA_OPTS="--add-modules=java.xml.bind strongbox"
ENV JAVA_FLAGS="-Djdk.net.URLClassPath.disableClassPathURLCheck=true"

RUN mvn install -f /home/schema_repo/

# USER schema_repo
CMD /home/schema_repo/run.sh "file-system"